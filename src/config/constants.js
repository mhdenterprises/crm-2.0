module.exports = {
  ERROR: "error",
  SUCCESS: "success",
  WARNING: "warning",
  DROPSHIP_STRING: "Dropship Requested",
};
