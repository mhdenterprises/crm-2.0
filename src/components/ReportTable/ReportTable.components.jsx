import React from "react";
import { Table } from "antd";
import moment from "moment";

const ReportTable = ({ notes, placelderText }) => {
  const columns = [
    {
      title: "Created At",
      dataIndex: "created_date",
      key: "created_date",
      render: (data) => (
        <div style={style.dateWidth}>
          {/* {moment.utc(data).format("Do MMM'YY, h:mm a")} */}
          {moment.utc(data).format("lll")}
        </div>
      ),
    },
    {
      title: "Added by",
      dataIndex: "created_user",
      key: "created_user",
    },
    {
      title: "Note Description",
      dataIndex: "note_description",
      key: "note_description",
    },
  ];
  return (
    <Table
      columns={columns}
      dataSource={notes}
      style={{ minWidth: "100%", margin: "10px 20px 0px" }}
    />
  );
};

export default ReportTable;

const style = {
  dateWidth: {
    width: 170,
    maxWidth: 170,
  },
};
