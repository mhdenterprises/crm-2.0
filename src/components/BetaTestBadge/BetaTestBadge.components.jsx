import React from "react";
import BetaTest1 from "../../assets/beta-test1.png";
const BetaTestBadge = ({ relative }) => {
  return (
    <div className={`beta-badge-box ${relative ? "prelative" : ""}`}>
      <img
        src={BetaTest1}
        alt="dexter testtube"
        className="dexter-badge-image"
      />
    </div>
  );
};

export default BetaTestBadge;
