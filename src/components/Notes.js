import React, { useCallback, useEffect, useState } from "react";
import { Form, Input } from "antd";
import AbstractButton from "./AbstractButton/AbstractButton.components";
import ReportTable from "./ReportTable/ReportTable.components";
import constants from "../config/constants";

// List of props: wo , CSRName, selectID, goBack, fetchNotes
export const Notes = ({ ...props }) => {
  const [notes, setNotes] = useState([]);
  const [error, setError] = useState("");
  const [note_description, set_note_description] = useState("");
  const [dropShip, setDropShip] = useState(false);

  const getNote = useCallback(() => {
    const data = {
      work_order_id: props.selectedID,
    };
    try {
      fetch("/getNotesByWorkOrderID", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data), // body data type must match "Content-Type" header
      })
        .then((response) => response.json())
        .then((data) => {
          if (data.error) {
            return;
          }
          data.notes.map((note) => {
            let isDropship = note.note_description.includes(
              constants.DROPSHIP_STRING
            );
            if (isDropship) {
              setDropShip(true);
            }
            return null;
          });
          setNotes(data.notes);
        });
    } catch (e) {
      setError(e.message);
    }
  }, [props.selectedID]);

  useEffect(() => {
    // Get Notes from here
    getNote();
  }, [getNote]);

  if (!notes) return <h5>No Notes Found</h5>;

  if (error) {
    return <h4>There is an error</h4>;
  }

  const addNote = (e) => {
    let data = {
      username: props.CSRName,
      note_description,
      wo_id: props.wo.id,
    };
    try {
      fetch("/addNote", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data), // body data type must match "Content-Type" header
      })
        .then((response) => response.json())
        .then((data) => {
          set_note_description("");
          getNote();
        });
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div
      style={{
        flex: 3,
      }}
    >
      <div style={{ margin: "0px 20px 20px" }}>
        <Form>
          <Form.Item label="Note">
            <div>
              <Input.TextArea
                style={{
                  fontSize: 16,
                  padding: 5,
                  borderRadius: 8,
                  width: "100%",
                  fontFamily: "Roboto",
                }}
                autoSize
                value={note_description}
                onChange={(e) => set_note_description(e.target.value)}
              />
            </div>
          </Form.Item>
          <Form.Item>
            <div>
              <AbstractButton
                text="Add Note"
                style={{ padding: 5, fontSize: 20, borderRadius: 8 }}
                className="custom-button"
                onClick={addNote}
                disabled={
                  props.CSRName.length === 0 ||
                  !props.wo.id ||
                  !note_description
                }
              />
            </div>
          </Form.Item>
        </Form>
      </div>
      <div style={{ position: "relative" }}>
        {dropShip && <p style={positionRight}>Drop Ship Requested</p>}
        <ReportTable notes={notes} />
      </div>
    </div>
  );
};

let positionRight = {
  position: "absolute",
  right: 80,
  top: -20,
  fontWeight: 600,
  zIndex: 5,
  color: "red",
  fontStyle: "italic",
};
