import React from "react";
import Notification from "../Notification/Notification.components";
import { withRouter } from "react-router-dom";
import AbstractButton from "../AbstractButton/AbstractButton.components";

const Logout = ({ history, loggedInUserName }) => {
  const onLogout = () => {
    localStorage.removeItem("loggedInUser");
    Notification("success", `Successfully Logged out`);
    history.push({ pathname: "/login" });
  };

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <div
        style={{ color: "white", marginRight: 20 }}
        className="logout-username"
      >
        {loggedInUserName}
      </div>
      <AbstractButton
        text={"Logout"}
        onClick={onLogout}
        className="custom-button"
      />
    </div>
  );
};

export default withRouter(Logout);
