import React from "react";
import { Input } from "antd";

const FormInput = ({
  placeholderText,
  defaultValueText,
  changeHandler,
  style,
  textArea = false,
  value,
}) =>
  textArea ? (
    <Input.TextArea
      size="middle"
      placeholder={placeholderText}
      defaultValue={defaultValueText}
      onChange={changeHandler}
      style={style}
      value={value}
    />
  ) : (
    <Input
      size="middle"
      placeholder={placeholderText}
      defaultValue={defaultValueText}
      onChange={changeHandler}
      value={value}
    />
  );

export default FormInput;
