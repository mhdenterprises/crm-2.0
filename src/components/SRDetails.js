import React from "react";
import { Notes } from "./Notes";
import CardDescriptions from "./CardDescriptions/CardDescriptions.components";

export const SRDetails = (props) => (
  <div
    style={{
      display: "flex",
      flexDirection: "row",
      padding: 10,
    }}
  >
    <CardDescriptions wo={props.wo} selectedOption={props.selectedOption} goBack={props.goBack} />
    <Notes {...props} />
  </div>
);
