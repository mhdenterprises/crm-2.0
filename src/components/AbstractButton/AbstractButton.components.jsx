import React from "react";
import "antd/dist/antd.css";
import { Button } from "antd";

const AbstractButton = ({ text, onClick, disabled = false, className }) => {
  return (
    <Button
      type="primary"
      onClick={onClick}
      disabled={disabled}
      className={className}
    >
      {text}
    </Button>
  );
};

export default AbstractButton;
