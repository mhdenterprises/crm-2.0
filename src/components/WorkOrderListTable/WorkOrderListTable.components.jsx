import React from "react";
import { Table, Space } from "antd";
import { SubStatusName } from "../../utils/SubStatusName";
import moment from "moment";
const { Column } = Table;

const WorkOrderListTable = ({ data, onClick }) => {
  return (
    <Table dataSource={data}>
      <Column
        title="SR#"
        dataIndex="po_sr"
        key="po_sr"
        render={(sr_num, record) => {
          return (
            <div
              style={record.rma ? { ...rmaSrStyle } : { ...normalSrStyle }}
              onClick={() => onClick(record)}
            >
              {sr_num}
            </div>
          );
        }}
      />
      <Column
        title="Created At"
        dataIndex="created_date"
        key="created_date"
        render={(data) => (
          <div>{moment.utc(data).format("Do MMM YYYY, h:mm a")}</div>
        )}
      />
      <Column title="Name" dataIndex="full_name" key="full_name" />
      <Column title="Phone" dataIndex="primary_phone" key="primary_phone" />
      <Column title="Email" dataIndex="email" key="email" />
      <Column
        title="Sub-Status"
        dataIndex="wo_sub_status_code"
        key="wo_sub_status_code"
        render={(text, record) => (
          <Space size="middle">{SubStatusName(text)}</Space>
        )}
      />
    </Table>
  );
};

const normalSrStyle = {
  color: "royalblue",
  fontWeight: "700",
  cursor: "pointer",
};

const rmaSrStyle = {
  color: "red",
  fontWeight: "700",
  cursor: "pointer",
};

export default WorkOrderListTable;
