import React, { useState } from "react";
import { Modal, Button, message } from "antd";

const SOButtonModal = ({ id }) => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = async () => {
    // Toggle the sales_order_created to 0

    try {
      await fetch("/putSalesOrderButtonBack", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ id: id }), // body data type must match "Content-Type" header
      })
        .then((response) => response.json())
        .then(async (data) => {
          // Successfully Updated
          const key = "updatable";
          await message.loading({ content: "Loading...", key, duration: 1 });
          setTimeout(async () => {
            await message.success({ content: "Successful!", key, duration: 1 });
          }, 500);
        });
    } catch (e) {
      // There are errors updating
      console.log(e);
    }
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      <Button
        type="link"
        onClick={showModal}
        className="soButtonPutBack"
        color="red"
      >
        Put Sales Order button back
      </Button>
      <Modal
        title="Are you sure?"
        visible={isModalVisible}
        okText="Continue"
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <p>Only use this feature if you want to create a sales order again.</p>
        <p style={{ fontSize: 12 }}>
          Please contact the IT department, if you're not sure
        </p>
      </Modal>
    </>
  );
};

export default SOButtonModal;
