import React from "react";
import { Input } from "antd";

const { TextArea } = Input;

const TextAreaInput = ({ placeholder, clickHandler }) => {
  return (
    <TextArea
      placeholder={placeholder || "Write here..."}
      onClick={clickHandler}
    />
  );
};

export default TextAreaInput;
