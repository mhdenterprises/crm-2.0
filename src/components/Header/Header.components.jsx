import React from "react";
import { Typography } from 'antd';

const { Title } = Typography;

const Header = ({titleText}) => {
    return <Title>{titleText}</Title>
}

export default Header;