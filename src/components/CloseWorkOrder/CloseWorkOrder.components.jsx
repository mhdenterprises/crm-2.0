import React, { useState, useEffect } from "react";
import { Form, Button, Modal, Select, Input, notification } from "antd";
import optionsLabel from "./helpers/optionsLabel";
import { SubStatusName } from "../../utils/SubStatusName";
import { PostApi } from "../../utils/PostApi";

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
/* eslint-disable no-template-curly-in-string */

const validateMessages = {
  required: "${label} is required!",
  types: {
    email: "${label} is not a valid email!",
    number: "${label} is not a valid number!",
  },
  number: {
    range: "${label} must be between ${min} and ${max}",
  },
};

const CloseWorkOrder = ({ wo, updateSelectedWoData, updateWorkOrderList }) => {
  const [form] = Form.useForm();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [closingReasons, setClosingReasons] = useState([]);
  const [subStatus, setSubStatus] = useState("");
  const [closingReasonText, setClosingReasonText] = useState([]);
  const [error, setError] = useState("");
  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const onFinish = async ({ work_order }) => {
    // Validation
    // closing_reason and wo_sub_status , required
    // closing_reason_note, not required

    if (!work_order.closing_reason) {
      notification.error({
        message: "You must select sub status and reason.",
      });
      return { error: "Add a closing reason." };
    }
    let data = localStorage.getItem("loggedInUser");
    data = JSON.parse(data);
    let username = data.name.toUpperCase();

    let requestedObject = {
      ...work_order,
      id: wo.id,
      dealer: wo.dealer,
      rma: wo.rma,
      last_name: wo.last_name,
      username,
    };

    await PostApi("/closeWorkOrder", requestedObject);
    let updatedValues = {
      id: wo.id,
      wo_sub_status_code: work_order.wo_sub_status_code,
      wo_status_code: "CLSD",
      work_order_id: wo.work_order_id,
    };
    updatedValues.username = username;
    updateSelectedWoData(updatedValues);
    updateWorkOrderList(updatedValues);

    notification.success({
      message: "File Successfully Closed",
    });

    // Give a moment to close the modal
    setTimeout(() => {
      handleCancel();
    }, 1000);
  };

  const onChange = (val) => {
    form.setFieldsValue({
      work_order: {
        closing_reason: null,
        closing_reason_note: null,
      },
    });
    setSubStatus(val);
    getOptions(val);
  };

  useEffect(() => {
    try {
      fetch("/getClosingReason", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
      })
        .then((response) => response.json())
        .then((data) => {
          if (data.error) {
            return;
          }
          setClosingReasons(data.closingReasons);
        });
    } catch (e) {
      setError(e.message);
    }
  }, []);

  const getOptions = (val) => {
    let options = closingReasons.find(
      (reason) => reason.wo_sub_status_code === val
    );
    if (options) {
      const listOfOptions = optionsLabel(options.reason_text_list);
      setClosingReasonText(listOfOptions);
    }
  };

  return (
    <>
      <Button
        type="secondary"
        onClick={showModal}
        className="cancel-button prelative"
      >
        Close
      </Button>
      <Modal
        title={<div className="flex prelative">Close Work Order</div>}
        visible={isModalVisible}
        onCancel={handleCancel}
        footer={null}
      >
        <Form
          {...layout}
          name="nest-messages"
          headers={[]}
          size={"small"}
          onFinish={onFinish}
          initialValues={{
            work_order: {
              ...wo,
              wo_sub_status_code: "",
            },
          }}
          form={form}
          validateMessages={validateMessages}
        >
          <Form.Item name={["work_order", "po_sr"]} label="SR #">
            <Input disabled className="disabled-input" />
          </Form.Item>
          <Form.Item name={["work_order", "work_order_id"]} label="WO #">
            <Input disabled className="disabled-input" />
          </Form.Item>
          <Form.Item
            name={["work_order", "wo_sub_status_code"]}
            label="Sub Status"
          >
            <Select onChange={onChange}>
              {closingReasons.length &&
                closingReasons.map((subStatus) => {
                  return (
                    <Select.Option value={subStatus.wo_sub_status_code}>
                      {SubStatusName(subStatus.wo_sub_status_code)}
                    </Select.Option>
                  );
                })}
            </Select>
          </Form.Item>
          {subStatus && (
            <>
              <Form.Item name={["work_order", "closing_reason"]} label="Reason">
                <Select>
                  {closingReasonText.map((reason) => (
                    <Select.Option value={reason.label}>
                      {reason.label}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item
                name={["work_order", "closing_reason_note"]}
                label="Note"
              >
                <Input.TextArea autoSize maxLength={200} showCount allowClear />
              </Form.Item>
            </>
          )}
          <div className="modal-footer">
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
            <Button type="secondary" htmlType="button" onClick={handleCancel}>
              Cancel
            </Button>
          </div>
          {error && <p>{error}</p>}
        </Form>
      </Modal>
    </>
  );
};

export default CloseWorkOrder;
