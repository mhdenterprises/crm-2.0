const optionsLabel = (options) =>
  options.map((option) => ({
    label: option,
    value: option,
  }));
export default optionsLabel;
