import React, { useState } from "react";
import {
  Form,
  Input,
  InputNumber,
  Button,
  Modal,
  Switch,
  notification,
  Select,
} from "antd";
const { Option } = Select;

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
/* eslint-disable no-template-curly-in-string */

const validateMessages = {
  required: "${label} is required!",
  types: {
    email: "${label} is not a valid email!",
    number: "${label} is not a valid number!",
  },
  number: {
    range: "${label} must be between ${min} and ${max}",
  },
};

const EditWorkOrder = ({
  workOrderInfo,
  updateSelectedWoData,
  updateWorkOrderList,
}) => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  // price, // number
  // threshold, //number
  // mfg, // string
  // part, //string
  // model, // string
  // cpu_model, // string
  // hard_drive, //string
  // ssd_value, //number
  // ram, //string
  // lcd_size, //number (float)
  // touch_screen, //number 0,1
  // has_data_plan, // number 0,1
  // extra_information, //string

  const onFinish = (values) => {
    // This can be converted into a UTIL function, will be doing it later
    // Only update those values that are changed, this will help in
    // determining which values changed and will be added in the Notes
    const updatedValues = {};
    if (workOrderInfo.price !== values.user.price) {
      updatedValues.price = values.user.price;
    }
    if (workOrderInfo.threshold !== values.user.threshold) {
      updatedValues.threshold = values.user.threshold;
    }
    if (workOrderInfo.serial_number !== values.user.serial_number) {
      updatedValues.serial_number = values.user.serial_number;
    }
    if (workOrderInfo.mfg !== values.user.mfg) {
      updatedValues.mfg = values.user.mfg;
    }
    if (workOrderInfo.part !== values.user.part) {
      updatedValues.part = values.user.part;
    }
    if (workOrderInfo.model !== values.user.model) {
      updatedValues.model = values.user.model;
    }
    if (workOrderInfo.cpu_model !== values.user.cpu_model) {
      updatedValues.cpu_model = values.user.cpu_model;
    }
    if (workOrderInfo.hard_drive !== values.user.hard_drive) {
      updatedValues.hard_drive = values.user.hard_drive;
    }

    if (
      values.user.prod_type !==
      GetProductType(workOrderInfo.desktop, workOrderInfo.tablet)
    ) {
      updatedValues.desktop = values.user.prod_type === "desktop" ? 1 : 0;
      updatedValues.tablet = values.user.prod_type === "tablet" ? 1 : 0;
      if (values.user.prod_type === "laptop") {
        updatedValues.desktop = 0;
        updatedValues.tablet = 0;
      }
    }

    if (workOrderInfo.ssd_value !== values.user.ssd_value) {
      updatedValues.ssd_value = values.user.ssd_value;
    }
    if (workOrderInfo.ram !== values.user.ram) {
      updatedValues.ram = values.user.ram;
    }
    if (workOrderInfo.lcd_size !== values.user.lcd_size) {
      updatedValues.lcd_size = values.user.lcd_size;
    }
    if (workOrderInfo.touch_screen !== (values.user.touch_screen ? 1 : 0)) {
      updatedValues.touch_screen = values.user.touch_screen ? 1 : 0;
    }
    if (workOrderInfo.has_data_plan !== (values.user.has_data_plan ? 1 : 0)) {
      updatedValues.has_data_plan = values.user.has_data_plan ? 1 : 0;
    }
    if (
      workOrderInfo.extra_information !== values.user.extra_information &&
      values.user.extra_information
    ) {
      updatedValues.extra_information = values.user.extra_information;
    }

    if (Object.keys(updatedValues).length === 0) {
      return notification.error({
        description:
          "You forgot to make an update, please make sure you have different values entered",
        message: "Nothing Changed",
      });
    }

    updatedValues.id = workOrderInfo.id;
    let data = localStorage.getItem("loggedInUser");
    data = JSON.parse(data);
    let username = data.name.toUpperCase();
    updatedValues.username = username;
    // return;
    // Make an API call here
    try {
      fetch("/editCustomerUnit", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(updatedValues), // body data type must match "Content-Type" header
      })
        .then((response) => response.json())
        .then((data) => {
          if (data.error) {
            notification.error({
              message: data.error,
            });
            return;
          }
          // Call back to update selected work order
          const { id, ...updateWithoutId } = updatedValues;
          updateSelectedWoData(updateWithoutId);
          updateWorkOrderList(updatedValues);
          notification.success({
            message: "Update Successful",
          });

          // Give a moment to close the modal
          setTimeout(() => {
            handleCancel();
          }, 1000);
        });
    } catch (e) {
      notification.error({
        message: "Update Failed",
        description: e.message,
      });
    }
  };

  const onChange = (checked) => {
    // console.log("changed some shit " + checked);
  };

  return (
    <>
      <Button type="secondary" onClick={showModal} className="beta-button">
        Edit
      </Button>
      <Modal
        title={<div className="beta-button flex">Edit Work Order Details</div>}
        visible={isModalVisible}
        onCancel={handleCancel}
        footer={null}
      >
        <Form
          {...layout}
          name="nest-messages"
          initialValues={{
            user: {
              ...workOrderInfo,
              touchscreen: workOrderInfo.has_data_plan === 1 ? true : false,
              has_data_plan: workOrderInfo.has_data_plan === 1 ? true : false,
              prod_type: GetProductType(
                workOrderInfo.desktop,
                workOrderInfo.tablet
              ),
            },
          }}
          headers={[]}
          size={"small"}
          onFinish={onFinish}
          validateMessages={validateMessages}
        >
          <Form.Item name={["user", "price"]} label="Unit Price">
            <InputNumber />
          </Form.Item>
          <Form.Item name={["user", "threshold"]} label="Threshold">
            <InputNumber />
          </Form.Item>
          <Form.Item name={["user", "mfg"]} label="MFG">
            <Input />
          </Form.Item>
          <Form.Item name={["user", "part"]} label="Part">
            <Input />
          </Form.Item>
          <Form.Item name={["user", "prod_type"]} label="Product Type">
            <Select defaultValue="laptop">
              <Option value="laptop">Laptop</Option>
              <Option value="desktop">Desktop</Option>
              <Option value="tablet">Tablet</Option>
            </Select>
          </Form.Item>
          <Form.Item name={["user", "model"]} label="Model #">
            <Input />
          </Form.Item>
          <Form.Item name={["user", "cpu_model"]} label="CPU Model #">
            <Input />
          </Form.Item>
          <Form.Item name={["user", "hard_drive"]} label="Hard Drive">
            <Input />
          </Form.Item>
          <Form.Item name={["user", "ssd_value"]} label="SSD">
            <Input />
          </Form.Item>
          <Form.Item name={["user", "serial_number"]} label="Serial#">
            <Input />
          </Form.Item>
          <Form.Item name={["user", "ram"]} label="RAM">
            <Input />
          </Form.Item>
          <Form.Item name={["user", "lcd_size"]} label="LCD Size">
            <InputNumber />
          </Form.Item>
          <Form.Item
            name={["user", "extra_information"]}
            label="Additional Specs"
          >
            <Input.TextArea rows={3} />
          </Form.Item>
          <Form.Item
            name={["user", "touch_screen"]}
            label="Touch Screen"
            valuePropName="checked"
          >
            <Switch onChange={onChange} />
          </Form.Item>
          <Form.Item
            name={["user", "has_data_plan"]}
            label="Has Data Plan"
            valuePropName="checked"
          >
            <Switch onChange={onChange} />
          </Form.Item>
          <div className="modal-footer">
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
            <Button type="secondary" htmlType="button" onClick={handleCancel}>
              Cancel
            </Button>
          </div>
        </Form>
      </Modal>
    </>
  );
};

const GetProductType = (desktop, tablet) => {
  if (desktop === 0 && tablet === 0) {
    return "laptop";
  } else if (desktop) {
    return "desktop";
  } else {
    return "tablet";
  }
};

export default EditWorkOrder;
