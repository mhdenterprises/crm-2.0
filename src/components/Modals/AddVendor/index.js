import React, { useState } from "react";
import { Input, notification, Button, Modal } from "antd";

const AddVendor = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [vendorName, setvendorName] = useState('');

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {

    if(vendorName === '') {
      notification.warning({
        message: "Warning",
        description: "Vendor name required",
      });
    } else {
      const vendorInfo = {};
      vendorInfo.vendorname = vendorName;
      
      let data = localStorage.getItem("loggedInUser");
      data = JSON.parse(data);
      let username = data.username;
      vendorInfo.username = 'IT';

      // Make an API call here
      setIsLoading(true);
      try {
        fetch("/addVendor", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(vendorInfo), // body data type must match "Content-Type" header
        })
          .then((response) => response.json())
          .then((data) => {

            setIsLoading(false);
            if (data.error) {
              notification.error({
                message: data.error,
              });
              return;
            }
            
            notification.success({
              message: "Vendor added succussfully",
            });

            // Give a moment to close the modal
            setTimeout(() => {
              handleCancel();
            }, 1000);
          });
      } catch (e) {
        notification.error({
          message: "Vendor creation failed",
          description: e.message,
        });
        setIsLoading(false);
      }
    }
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setvendorName('');
  };

  return (
    <>
      <Button className="custom-button-green" onClick={showModal}>
        Add
      </Button>
      <div className="add-vendor-modal">
        <Modal
            visible={isModalVisible} centered
            title="Add a vendor"
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[
              <Button className="custom-button-green" loading={isLoading} onClick={handleOk}>
                Add
              </Button>,
            ]}
          >
            <Input value={vendorName} onChange={e => setvendorName(e.target.value)}/>
          </Modal>
        </div>
    </>
  );
};

export default AddVendor;
