import React from "react";
import SelectBox from "./SelectBox/SelectBox.components";
const persons = [
  "AILEEN DATOON",
  "AMBRUCE KISWA",
  "ALYSIA REA",
  "DHAVAL JARDOSH",
  "JACQUELINE MENDOZA",
  "JAHAYRA RAMON",
  "JAIRA GOMEZ",
  "JOSELYN PINO",
  "JUHI RANA",
  "JULEANNE LAGARDE",
  "JULIE LUEDKE",
  "LAUREL EVARTS",
  "LAY LEKHASOPHON",
  "MARIANA DIACONU",
  "MARY JENN LAGARDE",
  "MOHAMED MOUACI",
  "WENDY RUNGSIMA",
  "SAMANTHA TURNER",
];
export const PickCSR = ({ locked, onChange }) => {
  return (
    <SelectBox
      options={persons}
      placeholderText="Select a Person"
      disabled={locked}
      onChange={onChange}
    />
  );
};
