import React from "react";
import { Descriptions, Badge, Space } from "antd";
import { SubStatusName } from "../../utils/SubStatusName";
import SOButtonModal from "../SOButtonModal/SOButtonModal.component";
import Jasypt from "jasypt";
const jasypt = new Jasypt();
jasypt.setPassword("MHDERP");
const CardDescriptions = ({ wo, selectedOption }) => {
  const CardDescriptionsComponent =
    wo !== undefined ? (
      <Space direction="vertical" style={{ minWidth: 300, maxWidth: "25vw" }}>
        <Descriptions
          title="User Info"
          layout="horizontal"
          bordered
          size={"small"}
        >
          <Descriptions.Item label="Full Name" span={3}>
            <b>{wo.full_name}</b>
          </Descriptions.Item>
          <Descriptions.Item label="SR Number" span={3}>
            <b>{wo.po_sr}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Username" span={3}>
            <b>{wo.user_name}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Password" span={3}>
            <b>{jasypt.decrypt(wo.encrypted_password)}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Created Date" span={3}>
            <b>{wo.created_date && new Date(wo.created_date).toDateString()}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Status" span={3}>
            <b>
              {wo.wo_status_code ? (
                wo.wo_status_code === "OPN" ? (
                  <Badge count={5} status="success" text="Open" />
                ) : (
                  <Badge count={5} status="error" text="Closed" />
                )
              ) : (
                ""
              )}
            </b>
          </Descriptions.Item>
          <Descriptions.Item label="Sub-Status" span={3}>
            <b>{SubStatusName(wo.wo_sub_status_code)}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Is RMA" span={3}>
            <b>{wo.rma && wo.rma === 1 ? "Yes" : "No"}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Address" span={3}>
            <b>{wo.address_line}</b>
          </Descriptions.Item>
          <Descriptions.Item label="City" span={3}>
            <b>{wo.city}</b>
          </Descriptions.Item>
          <Descriptions.Item label="State" span={3}>
            <b>{wo.us_state}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Zip Code" span={3}>
            <b>{wo.postal_code}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Primary Phone" span={3}>
            <b>{wo.primary_phone}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Mobile Phone" span={3}>
            <b>{wo.mobile_phone}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Home Phone" span={3}>
            <b>{wo.home_phone}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Email" span={3}>
            <b>{wo.email}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Work Order ID" span={3}>
            <b>{wo.work_order_id}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Sales Order Created" span={3}>
            <b>{wo.sales_order_created ? "Created" : "Not Created"}</b>
          </Descriptions.Item>
        </Descriptions>

        <Descriptions
          title="Customer's Unit"
          bordered
          size={"small"}
          labelStyle={{ width: 140 }}
        >
          <Descriptions.Item label="Dealer" span={3}>
            <b>{wo.dealer}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Purchase Date" span={3}>
            <b>{wo.created_date && new Date(wo.created_date).toDateString()}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Unit Price" span={3}>
            <b>{wo.price}</b>
          </Descriptions.Item>
          <Descriptions.Item label="MFG" span={3}>
            <b>{wo.mfg}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Part" span={3}>
            <b>{wo.part}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Serial #" span={3}>
            <b>{wo.serial_number}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Model #" span={3}>
            <b>{wo.model}</b>
          </Descriptions.Item>
          <Descriptions.Item label="CPU Model #" span={3}>
            <b>{wo.cpu_model}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Hard Drive" span={3}>
            <b>{wo.hard_drive}</b>
          </Descriptions.Item>
          <Descriptions.Item label="SSD" span={3}>
            <b>{wo.ssd_value}</b>
          </Descriptions.Item>
          <Descriptions.Item label="RAM" span={3}>
            <b>{wo.ram}</b>
          </Descriptions.Item>
          <Descriptions.Item label="LCD Size" span={3}>
            <b>{wo.lcd_size}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Touchscreen" span={3}>
            <b>{wo.touch_screen ? (wo.touch_screen ? "Yes" : "No") : ""}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Has Data Plan" span={3}>
            <b>{wo.has_data_plan ? "Yes" : "No"}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Additional Specs" span={3}>
            <b>{wo.extra_information}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Threshold" span={3}>
            <b>{wo.threshold}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Price" span={3}>
            <b>{wo.price}</b>
          </Descriptions.Item>
        </Descriptions>
        <Descriptions
          title="Selected Replacement"
          layout="horizontal"
          bordered
          size={"small"}
        >
          <Descriptions.Item label="Inventory Part Number" span={3}>
            <b>{selectedOption.inventory_part_number}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Customer Selected" span={3}>
            <b>{selectedOption.customer_selected === 1 ? 'Yes' : 'No'}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Manufacturer" span={3}>
            <b>{selectedOption.mfg}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Model Number" span={3}>
            <b>{selectedOption.model_number}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Part Number" span={3}>
            <b>{selectedOption.part_number}</b>
          </Descriptions.Item>
          <Descriptions.Item label="LCD" span={3}>
            <b>{selectedOption.lcd}</b>
          </Descriptions.Item>
          <Descriptions.Item label="CPU Model" span={3}>
            <b>{selectedOption.cpu_model}</b>
          </Descriptions.Item>
          <Descriptions.Item label="CPU Speed" span={3}>
            <b>{selectedOption.cpu_speed}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Memory" span={3}>
            <b>{selectedOption.memory}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Hard Drive" span={3}>
            <b>{selectedOption.hdd}</b>
          </Descriptions.Item>
          <Descriptions.Item label="SSD" span={3}>
            <b>{selectedOption.ssd}</b>
          </Descriptions.Item>
          <Descriptions.Item label="OS" span={3}>
            <b>{selectedOption.os}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Version" span={3}>
            <b>{selectedOption.version}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Warranty" span={3}>
            <b>{selectedOption.warranty}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Type" span={3}>
            <b>{selectedOption.type}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Data Plan" span={3}>
            <b>{selectedOption.has_data_plan}</b>
          </Descriptions.Item>
          <Descriptions.Item label="Cost" span={3}>
            <b>{selectedOption.cost}</b>
          </Descriptions.Item>
        </Descriptions>
        <SOButtonModal id={wo.id} />
      </Space>
    ) : (
      ""
    );
  return CardDescriptionsComponent;
};

export default CardDescriptions;
