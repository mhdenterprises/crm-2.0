import React from "react";
import WorkOrderSearch from "../../pages/WorkOrderSearch";
import { withRouter } from "react-router-dom";
import useAuth from "../../hooks/useAuth";

const AuthRoute = ({ history }) => {
  let { loading, loggedInUserInfo } = useAuth(history);

  if (loading) {
    return null;
  }

  if (!loggedInUserInfo) {
    return <h1>{""}</h1>;
  }

  return <WorkOrderSearch userId={loggedInUserInfo.name} />;
};

export default withRouter(AuthRoute);
