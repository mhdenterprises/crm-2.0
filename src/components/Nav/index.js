import React from "react";
import { Link } from "react-router-dom";
import Logout from "../Logout";
import useAuth from "../../hooks/useAuth";

const Nav = ({ history }) => {
  let { loggedInUserInfo } = useAuth(history);
  const style = {
    navbar: {
      background: "#140035",
      margin: -30,
      marginBottom: 30,
      flexDirection: "row",
      display: "flex",
      padding: "0px 20px",
      justifyContent: "space-between",
    },
    navItem: {
      padding: 20,
      color: "white",
      fontWeight: 600,
    },
  };

  const { navbar, navItem } = style;
  return (
    <nav style={navbar}>
      <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <Link to="/" style={navItem}>
            Customer Details
          </Link>
          <Link to="/settings" style={navItem}>
            Settings
          </Link>
        </div>
        <Logout loggedInUserName={loggedInUserInfo !== null ? loggedInUserInfo.name : ''} />
      </nav>
  );
};

export default Nav;
