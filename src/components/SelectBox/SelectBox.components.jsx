import React from "react";
import { Select } from "antd";

const { Option } = Select;

const SelectBox = ({
  options,
  placeholderText,
  disabled = false,
  onChange,
}) => {
  return (
    <div>
      <Select
        size="large"
        style={{ width: 200, marginLeft: 10 }}
        placeholder={placeholderText}
        disabled={disabled}
        onChange={onChange}
      >
        {options.map((option, key) => (
          <Option key={key} value={option}>
            {option}
          </Option>
        ))}
      </Select>
    </div>
  );
};

export default SelectBox;
