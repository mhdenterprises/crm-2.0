import { notification } from "antd";

const Notification = (type, text) =>
  notification[type]({
    message: "MHD CRM",
    description: text,
  });

export default Notification;
