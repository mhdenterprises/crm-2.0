import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Reports from "./pages/Reports";
import AuthRoute from "./components/AuthRoute/AuthRoute.component";
import Login from "./pages/Login";
import Settings from "./pages/Settings";

export default function App() {
  return (
    <Router>
      {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
      <Switch>
        <Route exact path="/">
          <AuthRoute />
        </Route>
        <Route path="/reports">
          <Reports />
        </Route>
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/settings">
          <Settings />
        </Route>
      </Switch>
    </Router>
  );
}
