import React from "react";
import useAuth from "../hooks/useAuth";
import Nav from "../components/Nav";
import { Button, Col, Form, Row, Space } from "antd";
import AddVendor from "../components/Modals/AddVendor";
import { withRouter } from "react-router-dom";

const Settings = ({history}) => {
  let { loading } = useAuth(history);

  if (loading) {
    return <h1>Loading</h1>;
  }

  const handleDealerClick = (e) => {
    if (e) {
      e.preventDefault();
    }
  }

  return (
    <div style={{ margin: 30, paddingBottom: 30 }}>
      <Nav/>
      <div style={style.outerBox} className="shadow add-vendor">
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <div style={{ padding: 10, flex: 1, display: "flex" }}>
            <Space direction="vertical" style={{ display: "flex" }}>
              <Form
                style={{
                  width: 1000,
                }}
              >
                <Row gutter={16}>
                  <Col md={16}>
                    <Form.Item style={{fontSize: 24}} label="Add a vendor">
                      <AddVendor/>
                    </Form.Item>
                  </Col>
                </Row>
                <Row gutter={16}>
                  <Col md={16}>
                    <Form.Item style={{fontSize: 24}} label="Add a dealer">
                    <Button
                        type="secondry"
                        htmlType="submit"
                        onClick={handleDealerClick}
                        className="custom-button-green"
                      >
                        Add
                      </Button>
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </Space>

          </div>
        </div>
      </div>                   
     </div>
  );
};

export default withRouter(Settings);

const style = {
  outerBox: {
    padding: 20,
    borderRadius: 10,
    background: "white",
  },
  labelStyle: {
    fontSize: 24,
  }
};
