import React, { useState } from "react";
import { Form, Input, Button } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { Row, Col } from "antd";
import { withRouter } from "react-router-dom";
import Notification from "../components/Notification/Notification.components.jsx";
import { ERROR, SUCCESS } from "../config/constants";
import useAuth from "../hooks/useAuth.js";
import FastLogo from "../assets/fast-arrow.png";

const Login = ({ history }) => {
  const [userId, setUserId] = useState("");
  const [password, setPassword] = useState("");
  let { loggedInUserInfo } = useAuth(history);

  if (loggedInUserInfo) {
    history.push("/");
  }

  const onFinish = async (values) => {
    let body = {
      userId,
      password,
    };

    const response = await fetch("/loginUser", {
      method: "POST", // *GET, POST, PUT, DELETE, etc.
      mode: "cors", // no-cors, *cors, same-origin
      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
      headers: {
        "Content-Type": "application/json",
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: "follow", // manual, *follow, error
      referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
      body: JSON.stringify(body), // body data type must match "Content-Type" header
    });
    const data = await response.json();
    if (data.status === "VALID") {
      Notification(SUCCESS, `Welcome ${data.name}`);
      localStorage.setItem("loggedInUser", JSON.stringify(data));
      history.push("/");
    } else {
      Notification(ERROR, `${data.error || "Password Incorrect"}`);
    }
  };

  return (
    <div className="login-page">
      <div className="login-card shadow">
        <div className="title-placement">
          <h1 className="logo-font">MHD CRM</h1>
          <img src={FastLogo} alt="fast track" className="fast-arrow" />
        </div>
        <Row style={{ margin: "20px 40px" }}>
          <Col span={24}>
            <Form
              name="login"
              className="login-form"
              initialValues={{
                remember: true,
                username: userId,
                password: password,
              }}
              onFinish={onFinish}
            >
              <Form.Item
                name="username"
                rules={[
                  {
                    required: true,
                    message: "Please input your Username!",
                  },
                ]}
              >
                <Input
                  prefix={<UserOutlined className="site-form-item-icon" />}
                  className="input-box"
                  placeholder="Username"
                  onChange={(e) => setUserId(e.target.value)}
                />
              </Form.Item>
              <Form.Item
                name="password"
                rules={[
                  {
                    required: true,
                    message: "Please input your Password!",
                  },
                ]}
              >
                <Input
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  className="input-box"
                  type="password"
                  placeholder="Password"
                  onChange={(e) => setPassword(e.target.value)}
                />
              </Form.Item>
              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="gradient-button full-width gradient-button-4"
                >
                  Log in
                </Button>
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default withRouter(Login);
