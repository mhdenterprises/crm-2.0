import React, { useEffect, useState } from "react";
import { Button, Col, Form, Row, Space, Spin } from "antd";
import FormInput from "../components/FormInput/FormInput.components";
import { SRDetails } from "../components/SRDetails";
import WorkOrderListTable from "../components/WorkOrderListTable/WorkOrderListTable.components";
import "../App.css";
import { Select } from "antd";
import Nav from "../components/Nav";
import EditWorkOrder from "../components/Modals/EditWorkOrder";
import CloseWorkOrder from "../components/CloseWorkOrder/CloseWorkOrder.components";

const { Option } = Select;

const WorkOrderSearch = ({ userId }) => {
  const [poNumber, setPoNumber] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [dealer, setDealer] = useState("");
  const [error, setError] = useState("");
  const [workOrderId, setWorkOrderId] = useState("");
  const [loading, setLoading] = useState(false);
  const [workOrderList, setWorkOrderList] = useState([]);
  const [selectedID, setSelectedID] = useState("");
  const [selectedWoData, setSelectedWoData] = useState({});
  const [woStatus, setWoStatus] = useState("");
  const [woSubStatus, setWoSubStatus] = useState("");
  const [dealerList, setDealerList] = useState([]);
  const [selectedOption, setSelectedOption] = useState({});

  useEffect(() => {
    const closeConnection = () => {
      fetch("/closeConnection");
    };

    const getDealerList = () => {
      fetch("/dealerList", {
        method: "POST",
      })
        .then((data) => data.json())
        .then((res) => setDealerList(res.dealers));
    };

    getDealerList();

    return () => closeConnection();
  }, []);

  const updateSelectedWoData = (updatedValues) => {
    let newSelectedWOData = JSON.parse(
      JSON.stringify({ ...selectedWoData, ...updatedValues })
    );
    setSelectedWoData(newSelectedWOData);
  };

  const updateWorkOrderList = (updatedValues) => {
    let clonedList = JSON.parse(JSON.stringify(workOrderList));
    let foundIndex = clonedList.findIndex((x) => x.id === updatedValues.id);
    clonedList[foundIndex] = { ...clonedList[foundIndex], ...updatedValues };
    setWorkOrderList(clonedList);
  };

  const resetSearch = () => {
    setPoNumber("");
    setFirstName("");
    setLastName("");
    setEmail("");
    setPhone("");
    setWorkOrderId("");
    setDealer("");
    setSelectedOption({})
  };

  const handleClick = (e) => {
    if (e) {
      e.preventDefault();
    }
    setSelectedID("");
    setError("");
    setLoading(true);
    const data = {
      po_sr: poNumber,
      first_name: firstName,
      last_name: lastName,
      email,
      primary_phone: phone,
      woSubStatus,
      woStatus,
      workOrderId,
      dealer,
    };

    try {
      fetch("/getWorkOrderList", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data), // body data type must match "Content-Type" header
      })
        .then((response) => response.json())
        .then((data) => {
          if (data.error) {
            setError(data.error);
          }
          setWorkOrderList(data.wo_detail);
          setLoading(false);
        });
    } catch (e) {
      setError(e.message);
      setLoading(false);
    }
  };

  const getSelectedOption = (id) => {
    // look to see if replacement was chosen by CM.
    // if none found, returns empty array
    fetch("/getSelectedOption", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({id: id}),
    })
    .then((response) => response.json())

    .then((response)=>{
      setSelectedOption({ ...response.selected_option[0] })
    })
  }

  const getUserNotes = (data) => {
    getSelectedOption(data.id)
    setSelectedID(data.id);
    setSelectedWoData(data);
  };

  const goBack = () => {
    setSelectedID("");
  };



  return (
    <div style={{ margin: 30, paddingBottom: 30 }}>
      <Nav/>
      <div style={style.outerBox} className="shadow">
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <div style={{ padding: 10, flex: 1, display: "flex" }}>
            <Space direction="vertical" style={{ display: "flex" }}>
              <Form
                onSubmitCapture={handleClick}
                style={{
                  width: 1000,
                }}
              >
                <Row gutter={16}>
                  <Col md={6}>
                    <Form.Item label="SR Number">
                      <FormInput
                        placeholderText="Enter SR number here"
                        value={poNumber}
                        changeHandler={(e) => setPoNumber(e.target.value)}
                        style={style.inputStyle}
                      />
                    </Form.Item>
                  </Col>
                  <Col md={8}>
                    <Form.Item label="Email" style={{ marginBottom: 20 }}>
                      <FormInput
                        placeholderText="Enter customer email"
                        value={email}
                        changeHandler={(e) => setEmail(e.target.value)}
                        style={style.inputStyle}
                      />
                    </Form.Item>
                  </Col>

                  <Col md={10}>
                    <Form.Item
                      label="Dealer"
                      style={{
                        marginBottom: 20,
                        marginLeft: 30,
                      }}
                    >
                      {dealerList && dealerList.length && (
                        <Select
                          defaultValue=""
                          style={{ width: "100%" }}
                          onChange={(val) => setDealer(val)}
                        >
                          <Option value={""}>All</Option>
                          {dealerList.map((dealer, index) => (
                            <Option value={dealer.name} key={index}>
                              {dealer.client_name}
                            </Option>
                          ))}
                        </Select>
                      )}
                    </Form.Item>
                  </Col>

                  <Col md={8}>
                    <Form.Item label="First Name" style={{ marginBottom: 20 }}>
                      <FormInput
                        placeholderText="Customer first name"
                        value={firstName}
                        changeHandler={(e) => setFirstName(e.target.value)}
                        style={style.inputStyle}
                      />
                    </Form.Item>
                  </Col>

                  <Col md={8}>
                    <Form.Item label="Last Name" style={{ marginBottom: 20 }}>
                      <FormInput
                        placeholderText="Customer last name"
                        value={lastName}
                        changeHandler={(e) => setLastName(e.target.value)}
                        style={style.inputStyle}
                      />
                    </Form.Item>
                  </Col>
                  <Col md={8}>
                    <Form.Item label="WO Id" style={{ marginBottom: 20 }}>
                      <FormInput
                        placeholderText="Work Order Id"
                        value={workOrderId}
                        changeHandler={(e) => setWorkOrderId(e.target.value)}
                        style={style.inputStyle}
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col md={8}>
                    <Form.Item
                      label="Phone Number"
                      style={{ marginBottom: 20 }}
                    >
                      <FormInput
                        placeholderText="Customer phone number"
                        value={phone}
                        changeHandler={(e) => setPhone(e.target.value)}
                        style={style.inputStyle}
                      />
                    </Form.Item>
                  </Col>
                  <Col span={16}>
                    <Row gutter={8}>
                      <Col span={10}>
                        <Form.Item
                          label="Status"
                          style={{ marginBottom: 20, marginLeft: 30 }}
                        >
                          <Select
                            defaultValue=""
                            style={{ width: 120 }}
                            onChange={(val) => setWoStatus(val)}
                          >
                            <Option value="OPN">Open</Option>
                            <Option value="CLSD">Closed</Option>
                            <Option value="">All</Option>
                          </Select>
                        </Form.Item>
                      </Col>
                      <Col span={14}>
                        {woStatus === "OPN" && (
                          <Form.Item
                            label="Sub-Status"
                            style={{
                              marginBottom: 20,
                              marginLeft: 30,
                            }}
                          >
                            <Select
                              defaultValue=""
                              style={{ width: "100%" }}
                              onChange={(val) => setWoSubStatus(val)}
                            >
                              <Option value="">All</Option>
                              <Option value="CRT">Created</Option>
                              <Option value="1EM">1st Email</Option>
                              <Option value="2EM">2nd Email</Option>
                              <Option value="RORD">Replacement Ordered</Option>
                              <Option value="RCONF">
                                Replacement Confirmed
                              </Option>
                              <Option value="TMR">Manager Review</Option>
                              <Option value="PSH">Pending Shipment</Option>
                            </Select>
                          </Form.Item>
                        )}
                        {woStatus === "CLSD" && (
                          <Form.Item
                            label="Sub-Status"
                            style={{ marginBottom: 20, marginLeft: 30 }}
                          >
                            <Select
                              defaultValue=""
                              style={{}}
                              onChange={(val) => setWoSubStatus(val)}
                            >
                              <Option value="">All</Option>
                              <Option value="REJ">Rejected</Option>
                              <Option value="ESC">Escalated</Option>
                              <Option value="SHP">Shipped</Option>
                            </Select>
                          </Form.Item>
                        )}
                      </Col>
                    </Row>
                  </Col>
                </Row>

                <Row gutter={16}>
                  <Col style={{ display: "flex" }}>
                    <Form.Item>
                      <Button
                        type="primary"
                        htmlType="submit"
                        onClick={handleClick}
                        className="custom-button"
                      >
                        Submit
                      </Button>
                    </Form.Item>
                  </Col>
                  <Col style={{ display: "flex" }}>
                    <Form.Item>
                      <Button
                        type="secondary"
                        htmlType="reset"
                        onClick={resetSearch}
                      >
                        Clear
                      </Button>
                    </Form.Item>
                  </Col>
                  {loading && <Spin size="large" style={{ marginLeft: 20 }} />}
                </Row>
              </Form>
            </Space>
          </div>
        </div>

        {error && !selectedID ? (
          <h4 style={{ color: "red", paddingLeft: 20 }}>{error}</h4>
        ) : (
          !selectedID && (
            <div>
              <p style={{ fontWidth: "500", fontStyle: "italic" }}>
                <b>{workOrderList.length}</b> records found
              </p>

              <WorkOrderListTable data={workOrderList} onClick={getUserNotes} />
            </div>
          )
        )}

        {error ? (
          <h4 style={{ color: "red", paddingLeft: 20 }}>{error}</h4>
        ) : (
          selectedID && (
            <div>
              <Space className="options-bar">
                <Button type="secondary" onClick={goBack}>
                  Go Back
                </Button>
                <div>
                  <Space>
                    <EditWorkOrder
                      workOrderInfo={selectedWoData}
                      updateSelectedWoData={updateSelectedWoData}
                      updateWorkOrderList={updateWorkOrderList}
                    />

                    {selectedWoData.wo_status_code !== "CLSD" && (
                      <CloseWorkOrder
                        wo={selectedWoData}
                        updateSelectedWoData={updateSelectedWoData}
                        updateWorkOrderList={updateWorkOrderList}
                      />
                    )}
                  </Space>
                </div>
              </Space>

              <SRDetails
                wo={selectedWoData}
                selectedOption={selectedOption}
                CSRName={userId.toUpperCase()}
                selectedID={selectedID}
                goBack={goBack}
              />
            </div>
          )
        )}
      </div>
    </div>
  );
};

export default WorkOrderSearch;

const style = {
  inputStyle: {
    fontSize: 30,
    marginBottom: 20,
  },
  outerBox: {
    padding: 20,
    borderRadius: 10,
    background: "white",
  },
  navbar: {
    background: "#140035",
    margin: -30,
    marginBottom: 30,
    flexDirection: "row",
    display: "flex",
    padding: "0px 20px",
    justifyContent: "space-between",
  },
  navItem: {
    padding: 20,
    color: "white",
    fontWeight: 600,
  },
};
