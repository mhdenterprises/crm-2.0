import React from "react";
import useAuth from "../hooks/useAuth";
import { withRouter } from "react-router-dom";

const Reports = ({ history }) => {
  let { loading, username, isLoggedIn } = useAuth();

  if (loading) {
    return <h1>Loading</h1>;
  }

  if (!isLoggedIn) {
    history.push("/login");
  }

  return <div>{username}</div>;
};

export default withRouter(Reports);
