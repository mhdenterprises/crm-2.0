import { useEffect, useState } from "react";
const useAuth = (history) => {
  const [loading, setLoading] = useState(false);
  const [loggedInUserInfo, setLoggedInUserInfo] = useState(null);
  const [userId, setUserId] = useState("");
  useEffect(() => {
    const fetchFromAsync = () => {
      setLoading(true);
      let data = localStorage.getItem("loggedInUser");
      data = JSON.parse(data);
      if (!data) {
        history.push("/login");
      }
      if (data) {
        setLoggedInUserInfo(data);
        setUserId(data.username);
      }
      setLoading(false);
    };
    fetchFromAsync();
  }, [history]);

  return {
    loading,
    loggedInUserInfo,
    userId,
  };
};

export default useAuth;
