import React from "react";
export const ShowValue = ({ label, value }) => {
  return (
    <div style={style.box}>
      <div style={style.label}>
        <div style={style.labelText}>{label}</div>
      </div>
      <div style={style.value}>
        <div>{value}</div>
      </div>
    </div>
  );
};

const style = {
  box: {
    display: "flex",
    paddingTop: 10,
  },
  label: {
    flex: 1,
    marginRight: 10,
  },
  labelText: { fontWeight: "600", textAlign: "right" },
  value: {
    flex: 2,
  },
};
