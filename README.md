# CRM 2.0

This project will be replacing the existing MHD CRM. It is built using React, AntD, Node and Express.

## Installation

Use the node package manager to install

```bash
npm install
```

This will install all the packages this project will need and will create `node_modules` and `package-lock.json`

## Usage

In **development**, you will need to start 2 servers simultaneously.

```javascript
npm start // Terminal 1, this will start the react project

NODE_ENV2=production node --tls-min-v1.0 server // start production environment server

node server // start development database server
```

In **production**, you will first need to build and then just need to run the server.

```
NODE_ENV2=production forever start server
```

At the moment we're using `NODE_ENV2` to decide the environment variables, but later will be planning something better.

## Auto-Documentation ([Swagger auto-gen](https://www.npmjs.com/package/swagger-autogen))

Documentation will be automatically created once add the appropriate routes.
You can access the documentation here `http://localhost:3001/doc`

Steps to add your route to the documentation:
After you have added a route (`post`, `put`, `delete`), here is what you need to do next.

1. Go to `swagger.js`, find `endPointsFiles`, and add the file path to that list
2. Go to the console/terminal and run `npm run swagger-autogen`, you should see status which says something like `Swagger-autogen: Success ✔`. If for some reason you see `Swagger-autogen: Failed ✖`, you should check the path if it's correct.
3. Now the file is updated and if you run the server with `node server`, you will see the documentation on the above mentioned route.

## Deployment

This project is depoyed on `192.168.10.241` and the path is `/apps/get-workorder-details`.

## Integration tests

This tests are to make sure all our api endpoints work as expected and they need to pass before doing a PR and doing a deployment. Adding additional API endpoints requires postman tests to be added.

Run `npm run integration`

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
