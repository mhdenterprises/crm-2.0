// Test specific configuration
// ===========================
module.exports = {
  crmSql: {
    server: "192.168.10.230",
    port: 50845,
    user: "CRMTEST",
    password: "09i6BcP9082t$423",
    database: "CRMTEST",
    options: {
      encrypt: true,
    },
  },
  schema: "CRMTEST",
  logFileName: "./server/log/test_logs/Log.log",
  AUTH_SECRET_PASSWORD: "MHDERP",
};
