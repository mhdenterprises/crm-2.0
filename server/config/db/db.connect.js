const mssql = require("mssql");
const env = process.env.NODE_ENV2 || "development";
const config = require(`../environment/${env}`);
const { crmSql } = config;

const connect = async () => await mssql.connect(crmSql);
module.exports = connect;
