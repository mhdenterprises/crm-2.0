const express = require("express");
const bodyParser = require("body-parser");
const pino = require("express-pino-logger")();
const sql = require("mssql");
const path = require("path");
const swaggerUi = require("swagger-ui-express");
const swaggerFile = require("./swagger/swagger-output.json");

const addNoteController = require("./api/controllers/notes/addNote.controller");
const getWorkOrderListController = require("./api/controllers/workOrder/getWorkOrderList.controller");
const getNotesByWorkOrderIDController = require("./api/controllers/notes/getNotesByWorkOrderID.controller");
const getWorkOrderBySRNumberController = require("./api/controllers/workOrder/getWorkOrderBySRNumber.controller");
const putSalesOrderButtonBackController = require("./api/controllers/salesOrder/putSalesOrderButtonBack.controller");
const loginUserController = require("./api/controllers/csr/loginUser.controller");
const dealerListController = require("./api/controllers/dealers/dealerList.controller");
const editCustomerUnitController = require("./api/controllers/workOrder/editCustomerUnit.controller");
const getClosingReasonController = require("./api/controllers/closingReason/getClosingReason.controller");
const closeWorkOrderController = require("./api/controllers/workOrder/closeWorkorder.controller");
const getCustomerInformationController = require("./api/controllers/customer/getCustomerInformation.controller");
const getReplacementOptionsController = require("./api/controllers/replacementOptions/getReplacementOptions.controller");
const getSelectedOptionController = require("./api/controllers/replacementOptions/getSelectedOption.controller");
const portalLoginController = require("./api/controllers/portal/portalLogin.controller");
const getShippingInformationController = require("./api/controllers/shipping/getShippingInformation.controller");
const addVendorController = require("./api/controllers/vendors/addVendor.controller");

const connectToDb = require("./config/db/db.connect");

const app = express();

app.use(pino);
app.use(bodyParser.json());

app.use("/doc", swaggerUi.serve, swaggerUi.setup(swaggerFile));

app.use(express.static(path.join(__dirname, "../build")));

connectToDb();

// Link CRM 2.0
app.get("/*", (req, res) => {
  res.sendFile(path.join(__dirname, "../build", "index.html"));
});

// Close Connection
app.post("/closeConnection", async (req, res) => {
  await sql.close();
  console.log("CONNECTION CLOSED");
});

// Get Work Order List with Search Parameters
app.post("/getWorkOrderList", getWorkOrderListController);

// Get Notes by Work Order ID
app.post("/getNotesByWorkOrderID", getNotesByWorkOrderIDController);

// Get Workorder by SR Number
app.post("/getWorkOrder", getWorkOrderBySRNumberController);

// Add Notes
app.post("/addNote", addNoteController);

// Put Sales Order Button Back
app.post("/putSalesOrderButtonBack", putSalesOrderButtonBackController);

// Login User
app.post("/loginUser", loginUserController);

// Get Dealer List
app.post("/dealerList", dealerListController);

// Edit Customer Unit
app.post("/editCustomerUnit", editCustomerUnitController);

// Get Closing Reasons
app.post("/getClosingReason", getClosingReasonController);

// Close Work Order
app.post("/closeWorkOrder", closeWorkOrderController);

// Get Customer Information
app.post("/getCustomerInformation", getCustomerInformationController);

// Portal Login
app.post("/portalLogin", portalLoginController);

// Get Replacement Options
app.post("/getReplacementOptions", getReplacementOptionsController);

// Get Selected Option
app.post("/getSelectedOption", getSelectedOptionController);

// Get shipping information
app.post("/getShippingInformation", getShippingInformationController);

// Add Vendor
app.post("/addVendor", addVendorController);

app.listen(3001, () =>
  console.log("Express server is running on localhost:3001")
);
