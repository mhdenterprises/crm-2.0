const Email = require("email-templates");
const nodemailer = require("nodemailer");
var path = require("path");
const env = process.env.NODE_ENV2 || "development";
const config = require(`../config/environment/${env}`);

/**
 * This function is responsible to create template with email-templates
 * and send it to single/multiple addresses
 * @param {*} group Partner Email / Customer Email / Status Push
 * @param {*} emailType S / WSH / RP / WRS / S
 * @param {String} sendTo This can take multiple addresses separated by ","
 * @param {String} sendFrom
 * @param {Object} emailContent Here is the email payload which we want to send
 */
module.exports = (
  group,
  emailType,
  sendTo,
  sendFrom,
  emailContent,
  bcc = ""
) => {
  //Transporter is needed to be sent via Mailgun
  var transporter = nodemailer.createTransport(config.transporter);

  //We need to manually change the path,
  //If we ignore this, you will get error saying
  //path to template not found
  const root = path.join(__dirname, group);

  //Creates the template
  const email = new Email({
    views: { root },
    message: {
      from: sendFrom,
    },
    // uncomment below to send emails in development/test env:
    send: true,
    //Transporter is required
    transport: transporter,
    preview: false,
  });

  //Send Email
  email
    .send({
      template: emailType,
      message: {
        to: sendTo,
        bcc,
      },
      locals: emailContent,
    })
    .then
    //TODO: This is where we will be changing the "completed": true
    //Will be based on id of the notification
    ()
    .catch((e) => console.log(e));
};
