// Either pass any date format, or just pass nothing and it will provide the current date
const sqlNormalizedDate = (date) => {
  return require("moment")(date || undefined).format("YYYY-MM-DD HH:mm:ss");
};

module.exports = { sqlNormalizedDate };
