const SubStatusName = (status) => {
  switch (status) {
    case "CRT":
      return "Created";
    case "TMR":
      return "Manager Review";
    case "PSH":
      return "Pending Shipment";
    case "1EM":
      return "1st Email";
    case "2EM":
      return "2nd Email";
    case "RORD":
      return "Replacement Ordered";
    case "RCONF":
      return "Replacement Confirmed";
    case "SHP":
      return "Shipped";
    case "REJ":
      return "Rejected";
    case "ESC":
      return "Escalated";
    default:
      return status;
  }
};

module.exports = SubStatusName;
