const Jasypt = require("jasypt");
const jasypt = new Jasypt();
const env = process.NODE_ENV2 || 'development';
const config = require(`../config/environment/${env}`);

jasypt.setPassword(config.AUTH_SECRET_PASSWORD);

const encrypt = plainText => jasypt.encrypt(plainText);
const decrypt = cipherText => jasypt.decrypt(cipherText);

module.exports = { encrypt, decrypt };