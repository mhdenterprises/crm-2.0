const arrangeBySubStatus = (reasonList) => {
  let result = [
    { wo_sub_status_code: "ESC", reason_text_list: [] },
    { wo_sub_status_code: "REJ", reason_text_list: [] },
  ];
  // eslint-disable-next-line array-callback-return
  reasonList.map((singleReason) => {
    if (singleReason.wo_sub_status_code === "ESC") {
      result[0].reason_text_list.push(singleReason.reason_text);
    }
    if (singleReason.wo_sub_status_code === "REJ") {
      result[1].reason_text_list.push(singleReason.reason_text);
    }
  });

  return result;
};

module.exports = arrangeBySubStatus;
