const formatQueryAndMessage = ({
  price, // number
  threshold, //number
  serial_number, // string
  mfg, // string
  part, //string
  model, // string
  cpu_model, // string
  hard_drive, //string
  ssd_value, //string
  ram, //string
  lcd_size, //number (float)
  touch_screen, //number 0,1
  has_data_plan, // number 0,1
  extra_information, //string
  desktop, // number 0,1
  tablet, // number 0,1
}) => {
  let query = `
    ${price ? `price=${price},` : ""}
    ${threshold ? `threshold=${threshold},` : ""}
    ${mfg ? `mfg='${mfg}',` : ""} 
    ${part ? `part='${part}',` : ""} 
    ${model ? `model='${model}',` : ""} 
    ${cpu_model ? `cpu_model='${cpu_model}',` : ""} 
    ${hard_drive ? `hard_drive='${hard_drive}',` : ""}
    ${ssd_value ? `ssd_value='${ssd_value}',` : ""} 
    ${serial_number ? `serial_number='${serial_number}',` : ""}
    ${ram ? `ram='${ram}',` : ""} 
    ${lcd_size ? `lcd_size=${lcd_size},` : ""}
    ${touch_screen !== undefined ? `touch_screen=${touch_screen},` : ""}
    ${has_data_plan !== undefined ? `has_data_plan=${has_data_plan},` : ""}
    ${desktop !== undefined ? `desktop=${desktop},` : ""}
    ${tablet !== undefined ? `tablet=${tablet},` : ""}
    ${
      extra_information
        ? `extra_information='${extra_information.replace(/'/g, "''")}',`
        : ""
    }`;

  // Adding to Notes String
  let note_description = `
      ${price ? `price,` : ""}
      ${threshold ? `threshold,` : ""}
      ${mfg ? `mfg,` : ""} 
      ${part ? `part,` : ""} 
      ${model ? `model,` : ""} 
      ${cpu_model ? `cpu_model,` : ""} 
      ${hard_drive ? `hard_drive,` : ""}
      ${ssd_value ? `ssd_value,` : ""} 
      ${serial_number ? `serial_number,` : ""} 
      ${ram ? `ram,` : ""} 
      ${lcd_size ? `lcd_size,` : ""}
      ${touch_screen !== undefined ? `touch_screen,` : ""}
      ${has_data_plan !== undefined ? `has_data_plan,` : ""}
      ${extra_information ? `extra_information,` : ""}
      ${desktop ? `desktop,` : ""}
      ${tablet ? `tablet,` : ""}`;
  query = query.trim();
  query = query.substring(0, query.length - 1);
  note_description = note_description.replace(/[ \r\n]+/g, "");
  note_description =
    "Changed " + note_description.substring(0, note_description.length - 1);

  return { query, note_description };
};

module.exports = formatQueryAndMessage;
