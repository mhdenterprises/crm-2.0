const updateWorkOrderInput = ({
  closed_date,
  closing_reason,
  wo_status_code,
  wo_sub_status_code,
  username,
}) => {
  let query = `
    ${closed_date ? `closed_date='${closed_date}',` : ""} 
    ${closing_reason ? `closing_reason='${closing_reason}',` : ""} 
    ${wo_status_code ? `wo_status_code='${wo_status_code}',` : ""} 
    ${wo_sub_status_code ? `wo_sub_status_code='${wo_sub_status_code}',` : ""}
    ${username ? `updated_date=GETDATE(), updated_user='${username}',` : ""}
    `;
  query = query.trim();
  query = query.substring(0, query.length - 1);
  return { query };
};

module.exports = updateWorkOrderInput;
