// Create a function here that will take in WorkorderDetail,
// and will make an API call to Asurion
const env = process.env.NODE_ENV2 || "development";
const config = require(`../config/environment/${env}`);
const fetch = require("node-fetch");

const statusPush = async function (body) {
  let url = config.asurionapiendpoint;
  let authToken = config.asurionapiauthtoken;
  let apiKey = config.asurionapikey;

  // API CALL
  const response = await fetch(url, {
    method: "POST",
    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
      Auth: authToken,
      "x-api-key": apiKey,
    },
    redirect: "follow",
    referrerPolicy: "no-referrer",
    body: JSON.stringify(body),
  });

  // RETURN RESPONSE
  return response.json(); // parses JSON response into native JavaScript objects
};

module.exports = statusPush;
