const sql = require("mssql");
const { MemoryRouter } = require("react-router");
const {
  getShippingInformationQuery,
} = require("../../sql/shipping/getShippingInformation.query");

const getShippingInformation = async (req, res) => {
  if (!req.body.work_order_number) {
    return res.status(400).send({ error: "work order number must be present" });
  }

  try {
    const { work_order_number: workOrderId } = req.body;
    const shippedInformationResult = await getShippingInformationQuery({
      workOrderId,
      sql,
    });
    if (shippedInformationResult.recordset.length > 0) {
      const {
        created_date,
        is_shipped,
        shipping_method,
        tracking,
        mfg,
        product_description,
        cpu_model,
        hdd,
        memory,
        lcd,
        first_name,
        last_name,
        address_line,
        second_address_line,
        city,
        us_state,
        postal_code,
      } = shippedInformationResult.recordset[0];
      return res.status(200).send({
        shipment_information: {
          created_date,
          is_shipped,
          shipping_method,
          tracking,
        },
        replacement_device: {
          mfg,
          product_description,
          cpu_model,
          hdd,
          memory,
          lcd,
        },
        shipping_address: {
          first_name,
          last_name,
          address_line,
          second_address_line,
          city,
          us_state,
          postal_code,
        },
      });
    }
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
};

module.exports = getShippingInformation;
