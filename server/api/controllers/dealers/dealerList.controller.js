const dealerListQuery = require("../../sql/dealers/dealerList.query");

const dealerListController = async (req, res) => {
  try {
    // Search for User Notes
    let dealersList = await dealerListQuery();
    let dealers = dealersList.recordset;

    // Send it
    res.status(200).send({ dealers });
  } catch (e) {
    res.status(405).send({ error: e.message });
  }
};

module.exports = dealerListController;
