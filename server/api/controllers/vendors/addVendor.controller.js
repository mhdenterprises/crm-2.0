const { sqlNormalizedDate } = require("../../../util/sqlNormalizedDate");
const  addVendorQuery = require("../../sql/vendors/addVendor.query");

const addVendorController = async (req, res) => {
  let username = req.body.username;
  let vendorname = req.body.vendorname;
  var normalizedDate = sqlNormalizedDate();

  if (!vendorname || !username) {
    return "username, vendorname are required parameters";
  }

  const data = {
    username,
    vendorname,
    normalizedDate
  };

  try {
    let result = await addVendorQuery(data);
    return res.status(200).send(result);
  } catch (e) {
    console.log(e.message);
    return res.status(400).send({ error: e.message });
  }
};

module.exports = addVendorController;
