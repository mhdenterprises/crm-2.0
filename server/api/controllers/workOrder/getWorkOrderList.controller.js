const getWorkOrderListQuery = require("../../sql/workOrder/getWorkOrderList.query");

const getWorkOrderListController = async (req, res) => {
  try {
    // Calling Query
    let wo_details = await getWorkOrderListQuery(req);

    // Build wo_detail and notes
    let wo_detail = wo_details.recordset;

    // Send it
    res.status(200).send({ wo_detail });
  } catch (err) {
    // ... error checks
    res.status(405).send({
      error: "Not a valid SR Number. Please try with a valid one.",
      wo_detail: [],
    });
  }
};

module.exports = getWorkOrderListController;
