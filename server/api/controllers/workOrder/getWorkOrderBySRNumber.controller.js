const getWorkOrderBySRNumber = require("../../sql/workOrder/getWorkOrderBySRNumber.query");

const getWorkOrderBySRNumberController = async (req, res) => {
  if (!req.body.po_sr) {
    return res.status(404).send({ error: 'PO is required(field name "po")' });
  }

  try {
    // Query
    const result = await getWorkOrderBySRNumber(req);

    // Send it
    res.status(200).send(result);
  } catch (err) {
    // ... error checks
    res.status(405).send({
      error: "Not a valid SR Number. Please try with a valid one.",
      wo_detail: [],
    });
  } finally {
  }
};

module.exports = getWorkOrderBySRNumberController;
