// We can change it to Edit Work Order, but for the sake of
// "name simplicity" we are keeping editCustomerUnit
const formatQueryAndMessage = require("../../../util/formatQueryAndMessage");
const { sqlNormalizedDate } = require("../../../util/sqlNormalizedDate");
const addNoteQuery = require("../../sql/notes/addNoteQuery");
const editCustomerUnitQuery = require("../../sql/workOrder/editCustomerUnit.query");

const editCustomerUnitController = async (req, res) => {
  // Validation
  // id should be present and should be a number
  if (!req.body.id || typeof req.body.id !== "number") {
    return res
      .status(400)
      .send({ error: "id must be present and should be a number." });
  }

  const data = {
    price: req.body.price,
    threshold: req.body.threshold,
    mfg: req.body.mfg,
    part: req.body.part,
    serial_number: req.body.serial_number,
    model: req.body.model,
    cpu_model: req.body.cpu_model,
    hard_drive: req.body.hard_drive,
    ssd_value: req.body.ssd_value,
    ram: req.body.ram,
    lcd_size: req.body.lcd_size,
    touch_screen: req.body.touch_screen,
    has_data_plan: req.body.has_data_plan,
    extra_information: req.body.extra_information,
    desktop: req.body.desktop,
    tablet: req.body.tablet,
  };

  let { query, note_description } = formatQueryAndMessage(data);

  // Input needed for Work Order
  let workOrderInput = {
    id: req.body.id,
    query,
  };

  let normalizedDate = sqlNormalizedDate();

  // Input needed for Confirmation Note
  let confirmationNoteInput = {
    wo_id: req.body.id,
    note_description,
    username: req.body.username,
    normalizedDate,
  };

  try {
    // Execution Editing
    let updatedQueryResult = await editCustomerUnitQuery(workOrderInput);

    // If 0 rows affected, when the id is incorrect
    if (updatedQueryResult.rowsAffected[0] === 0) {
      return res
        .status(404)
        .send({ error: "Work Order not found. Try again!" });
    }
    await addNoteQuery(confirmationNoteInput);
    // If everything successful, just send a successful status
    return res.status(200).send({ status: "Successful" });
  } catch (e) {
    return res.status(400).send({ error: e.message });
  }
};

module.exports = editCustomerUnitController;
