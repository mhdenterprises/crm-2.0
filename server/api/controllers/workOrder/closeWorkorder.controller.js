const emailJob = require("../../../emails/emailJob");
const { sqlNormalizedDate } = require("../../../util/sqlNormalizedDate");
const SubStatusName = require("../../../util/SubStatusName");
const updateWorkOrderInput = require("../../../util/updateWorkOrderInput");
const updateCustomerLogin = require("../../sql/customer/updateCustomerLogin.query");
const addNoteQuery = require("../../sql/notes/addNoteQuery");
const updateWorkOrder = require("../../sql/workOrder/updateWorkOrder.query");
const insertWoStatusPushQuery = require("../../sql/woStatusPush/insertWoStatusPush.query");
const statusPush = require("../../statusPush");
const env = process.env.NODE_ENV2 || "development";
const config = require(`../../../config/environment/${env}`);

const closeWorkOrderController = async (req, res) => {
  // Parameters:
  //
  // id*
  // closing_reason*
  // wo_sub_status_code*
  // closing_reason_note
  // dealer*
  // last_name*
  // po_sr*
  // work_order_id*
  // rma

  const {
    id,
    closing_reason,
    wo_sub_status_code,
    username,
    closing_reason_note,
    dealer,
    last_name,
    po_sr,
    work_order_id,
    rma,
  } = req.body;

  let wo_status_code = "CLSD";

  // INPUT VALIDATION
  if (
    !id ||
    !closing_reason ||
    !wo_sub_status_code ||
    !dealer ||
    !last_name ||
    !po_sr ||
    !work_order_id
  ) {
    return res.status(400).send({
      status: "Unsuccessful",
      error:
        "Required parameters: id(Number), closing_reason(String), wo_sub_status_code(String), dealer(String), last_name(String), po_sr(Number) and work_order_id(Number). Optional parameters: closing_reason_note(String) and rma(0 or 1)",
    });
  }

  try {
    let closed_date = sqlNormalizedDate();
    let woInputQuery = updateWorkOrderInput({
      id,
      closed_date,
      closing_reason,
      wo_status_code,
      wo_sub_status_code,
      username,
    });

    // UPDATE WORK ORDER
    await updateWorkOrder({
      id,
      query: woInputQuery.query,
    });

    // CREATE USERNOTE
    let note_description = `Closed Work Order: ${SubStatusName(
      wo_sub_status_code
    )} ${closing_reason ? `: ${closing_reason}` : ""} ${
      closing_reason_note ? ` - ${closing_reason_note}` : ""
    }`;

    await addNoteQuery({
      username,
      wo_id: id,
      note_description,
      normalizedDate: closed_date,
    });

    // UPDATE CUSTOMER LOGIN
    await updateCustomerLogin({
      po_sr,
      updated_user: username,
    });

    // ASURION STATUS PUSH
    let asurionDataObject = {
      version: config.asurionApiVersion,
      senderID: config.asurionApiSenderID,
      notificationType:
        wo_sub_status_code === "ESC" ? "Escalation" : "Rejected",
      escalationReason: closing_reason,
      notes: closing_reason_note || "",
      referenceID: po_sr, // SR NUMBER
      orderNumber: work_order_id, // WORK ORDER ID
    };

    await statusPush(asurionDataObject);

    // STATUS PUSH TO MHD DATABASE
    await insertWoStatusPushQuery({
      statusData: {
        username,
        closing_reason_note,
        closing_reason,
        wo_sub_status_code,
        dealer,
        po_sr,
        work_order_id,
      },
    });

    // SEND EMAILS
    const sendTo = config.statusEmailRecipient;
    const bccTo = config.bccStatusTrackingEmail;
    const sentFrom = config.statusEmailSentFrom;

    emailJob(
      "PartnerEmail",
      wo_sub_status_code[0].toUpperCase(), // Will select either E or R
      sendTo,
      sentFrom,
      {
        CLOSING_REASON: closing_reason,
        CLOSING_REASON_NOTE: closing_reason_note,
        dealer,
        SR_NUMBER: po_sr,
        LAST_NAME: last_name,
        RMA: rma,
      },
      bccTo
    );

    // SEND STATUS
    res.status(200).send({ status: "Successful" });
  } catch (e) {
    console.log("something here");
    console.log(e.message);
    res.status(200).send({ status: "error", error: e.message });
  }
};

module.exports = closeWorkOrderController;
