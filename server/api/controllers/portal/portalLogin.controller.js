const { decrypt } = require("../../../util/encryption");
const portalLoginQuery = require("../../sql/portal/portalLogin.query");

const portalLoginController = async (req, res) => {
  if (!req.body.username || !req.body.password || !req.body.portal_type) {
    return res
      .status(400)
      .send({ error: "username, password and portal type must be provided." });
  }

  try {
    const { username, password, portal_type } = req.body;
    const portalLoginResult = await portalLoginQuery({ username, portal_type });
    if (
      portalLoginResult &&
      portalLoginResult.recordset &&
      portalLoginResult.recordset.length > 0
    ) {
      const {
        encrypted_password,
        customer_id,
        work_order_id,
        first_name,
        last_name,
        address_line,
        second_address_line,
        city,
        us_state,
        postal_code,
        wo_sub_status_code,
      } = portalLoginResult.recordset[0];
      const decryptedPwd = decrypt(encrypted_password);
      if (decryptedPwd === password) {
        return res.status(200).send({
          work_order_id,
          work_order_status: wo_sub_status_code,
          customer: {
            id: customer_id,
            first_name,
            last_name,
            address_line,
            second_address_line,
            city,
            us_state,
            postal_code,
          },
        });
      } else {
        return res.status(401).send({
          error: `Username or password is incorrect. Please try again`,
        });
      }
    } else {
      return res
        .status(404)
        .send({ error: `Could not find login for user: ${username}` });
    }
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
};

module.exports = portalLoginController;
