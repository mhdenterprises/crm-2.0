const getCustomerInformationQuery = require("../../sql/customer/getCustomerInformation.query");

const getCustomerInformationController = async (req, res) => {
  if (!req.body.customer_id || typeof req.body.customer_id !== "number") {
    return res
      .status(400)
      .send({ error: "customer id must be present and should be a number." });
  }

  try {
    const { customer_id: customerId } = req.body;
    const customerInformationResult = await getCustomerInformationQuery({
      customerId,
    });
    if (customerInformationResult.recordset.length > 0) {
      const {
        first_name,
        last_name,
        address_line,
        second_address_line,
        city,
        us_state,
        postal_code,
        mfg,
        model,
        cpu_model,
        lcd_size,
        ram,
        hard_drive,
      } = customerInformationResult.recordset[0];
      return res.status(200).send({
        first_name,
        last_name,
        address_line,
        second_address_line,
        city,
        us_state,
        postal_code,
        device: {
          mfg,
          model,
          cpu_model,
          lcd_size,
          ram,
          hard_drive,
        },
      });
    }
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
};

module.exports = getCustomerInformationController;
