const Jasypt = require("jasypt");
const development = require("../../../config/environment/development");
const production = require("../../../config/environment/production");
const test = require("../../../config/environment/test");
const loginUserQuery = require("../../sql/csr/loginUser.query");

const jasypt = new Jasypt();
const loginUserController = async (req, res) => {
  let { userId, password } = req.body;

  // Validation
  if (!userId || !password) {
    return res.status(405).send({
      status: "INVALID",
      message: "User ID (userId) and Password (password) are required.",
    });
  }

  // Choose Config - To select for DB Creds
  let config = (env) => {
    switch (env) {
      case "production":
        return production;
      case "development":
        return development;
      case "test":
        return test;
      default:
        return development;
    }
  };

  // Get the Work Order List after the Query
  let userDetails = await loginUserQuery(userId);

  // Build wo_detail and notes
  let userDetail = userDetails.recordset;

  // If User not found
  if (!userDetail.length) {
    return res
      .status(404)
      .send({ error: "No User found with this user id - " + userId });
  }

  const configFile = config(process.env.NODE_ENV2);
  jasypt.setPassword(configFile.AUTH_SECRET_PASSWORD);

  const decryptedPassword = jasypt.decrypt(
    userDetail[0].encrypted_person_password
  );

  // Wrong Password
  if (password !== decryptedPassword) {
    return res.status(404).send({
      status: "INVALID",
    });
  }

  // Send it
  res.status(200).send({
    status: "VALID",
    username: userId,
    name: userDetail[0].full_name,
  });
};

module.exports = loginUserController;
