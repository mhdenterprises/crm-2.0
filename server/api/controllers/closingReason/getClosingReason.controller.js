const arrangeBySubStatus = require("../../../util/arrangeBySubStatus");
const getClosingReasonQuery = require("../../sql/closingReasons/getClosingReason.query");

const getClosingReasonController = async (req, res) => {
  try {
    // Execution Editing
    let closingReasonList = await getClosingReasonQuery();
    if (closingReasonList.recordsets[0]) {
      let closingReasons = closingReasonList.recordsets[0];
      let formattedList = arrangeBySubStatus(closingReasons);
      return res.status(200).send({ closingReasons: formattedList });
    }

    // If everything successful, just send a successful status
  } catch (e) {
    return res.status(400).send({ error: e.message });
  }
};

module.exports = getClosingReasonController;
