const putSOButtonBackQuery = require("../../sql/salesOrder/putSalesOrderButtonBack.query");

const putSalesOrderButtonBackController = async (req, res) => {
  if (!req.body.id) {
    return res.status(400).send({ error: "Id of Work Order is required!" });
  }
  try {
    await putSOButtonBackQuery({ id: req.body.id });
    return res.status(200).send({ status: "Successful" });
  } catch (e) {
    return res.status(400).send({ error: e.message });
  }
};

module.exports = putSalesOrderButtonBackController;
