const { sqlNormalizedDate } = require("../../../util/sqlNormalizedDate");
const addNoteQuery = require("../../sql/notes/addNoteQuery");
const sql = require("mssql");

const addNoteController = async (req, res) => {
  let username = req.body.username;
  let note_description = req.body.note_description;
  let wo_id = req.body.wo_id;
  var normalizedDate = sqlNormalizedDate();

  if (!wo_id || !note_description || !username) {
    return "username, wo_id and note_description are required parameters";
  }

  const data = {
    username,
    note_description,
    wo_id,
    normalizedDate,
    sql,
  };

  try {
    await addNoteQuery(data);
    return res.status(200).send({ status: "Successful" });
  } catch (e) {
    console.log(e.message);
    return res.status(400).send({ error: e.message });
  }
};

module.exports = addNoteController;
