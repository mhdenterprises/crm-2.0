const getNotesByWorkOrderIDQuery = require("../../sql/notes/getNotesByWorkOrderID.query");
const getNotesByWorkOrderIDController = async (req, res) => {
  if (!req.body.work_order_id) {
    return res.status(400).send({ error: "Work Order ID not found" });
  }

  try {
    // Search for User Notes
    let user_notes = await getNotesByWorkOrderIDQuery(req);
    let notes = user_notes.recordset;

    // Send it
    res.status(200).send({ notes });
  } catch (e) {
    res.status(405).send({ error: e.message });
  }
};

module.exports = getNotesByWorkOrderIDController;
