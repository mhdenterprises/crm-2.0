const getReplacementOptionsQuery = require("../../sql/replacementOptions/getReplacementOptions.query");

const getReplacementOptionsController = async (req, res) => {
  if (!req.body.work_order_id) {
    return res.status(400).send({ error: "work order is must be present" });
  }
  try {
    const { work_order_id: workOrderId } = req.body;
    const replacementOptionsResult = await getReplacementOptionsQuery({
      workOrderId,
    });
    res.send({ replacement_options: replacementOptionsResult.recordset });
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
};

module.exports = getReplacementOptionsController;
