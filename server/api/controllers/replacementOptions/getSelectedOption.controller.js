const getSelectedOptionQuery = require("../../sql/replacementOptions/getSelectedOption.query");

const getSelectedOptionController = async (req, res) => {
  if (!req.body.id) {
    return res.status(400).send({ error: "id must be present" });
  }
  try {
    const { id: id } = req.body;
    const selectedOptionResult = await getSelectedOptionQuery({
      id,
    });
    res.send({ selected_option: selectedOptionResult.recordset });
  } catch (error) {
    return res.status(500).send({ error: error.message });
  }
};

module.exports = getSelectedOptionController;
