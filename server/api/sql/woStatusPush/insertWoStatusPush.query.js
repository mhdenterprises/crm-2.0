const env = process.env.NODE_ENV2 || "development";
const config = require(`../../../config/environment/${env}`);
const sql = require("mssql");

const insertWoStatusPushQuery = ({ statusData }) => {
  return sql.query(`
  BEGIN TRANSACTION
  INSERT INTO wo_status_push
              (created_date,
              created_user,
              updated_date,
              updated_user,
              version,
              comments,
              escalation_reason,
              message_version,
              notification_type,
              program_name,
              request_time_stamp,
              sender_id,
              session_id,
              sr_number,
              status_push_flag,
              test_prod_flag,
              work_order_id)
  VALUES     ( Getdate(),
              '${statusData.username}' ,
              Getdate(),
              '${statusData.username}',
              1,
              ${
                statusData.closing_reason_note
                  ? "'" +
                    statusData.closing_reason_note.replace(/'/g, "''") +
                    "'"
                  : "NULL"
              },
              '${statusData.closing_reason}',
              1.05,
              ${
                statusData.wo_sub_status_code === "ESC"
                  ? "'Escalation'"
                  : "'Rejected'"
              },
              '${statusData.dealer}' ,
              Getdate(),
              'MHD',
              12,
              ${statusData.po_sr},
              1,
              '${config.statusPushTestProdFlag}',
              ${statusData.work_order_id}  )
  
  COMMIT
`);
};

module.exports = insertWoStatusPushQuery;
