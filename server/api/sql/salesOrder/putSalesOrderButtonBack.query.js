const sql = require("mssql");

const putSOButtonBackQuery = async ({ id }) => {
  return await sql.query`
      BEGIN TRANSACTION
      UPDATE work_order
      SET sales_order_created = 0
      WHERE id = ${id}
      COMMIT
      `;
};

module.exports = putSOButtonBackQuery;
