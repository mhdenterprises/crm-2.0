const getShippingInformationQuery = ({ workOrderId, sql }) => {
  return sql.query`
    SELECT 
    s.created_date, s.is_shipped, s.shipping_method, s.tracking,
    lp.mfg, lp.product_description, lp.cpu_model, lp.hdd, lp.memory, lp.lcd,
    cc.first_name, cc.last_name, acc.address_line, acc.second_address_line, acc.city, acc.us_state, acc.postal_code
    FROM shipped s
    INNER JOIN laptop_product lp
    ON lp.inventory_part_number = s.part_num
    INNER JOIN work_order wo
    ON wo.work_order_id = s.wo_num
    INNER JOIN customer_contact cc
    ON cc.customer_id = wo.customer_id
    INNER JOIN address_customer_contact acc
    ON acc.customer_contact_id = cc.id
    WHERE s.wo_num = ${workOrderId}
    AND s.ship_type = 1
    AND cc.primary_contact = 1
  `;
};

module.exports = { getShippingInformationQuery };
