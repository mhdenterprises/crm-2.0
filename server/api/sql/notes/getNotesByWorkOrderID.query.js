const sql = require("mssql");

const getNotesByWorkOrderIDQuery = async (req) => {
  // Parameter based entry isn't required here.
  return await sql.query(`
    SELECT * FROM user_note un 
    WHERE work_order_id = ${req.body.work_order_id}
    ORDER BY created_date DESC
  `);
};

module.exports = getNotesByWorkOrderIDQuery;
