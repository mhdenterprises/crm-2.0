const sql = require("mssql");
const addNoteQuery = async ({
  username,
  note_description,
  wo_id,
  normalizedDate,
}) => {
  console.log(username, note_description, wo_id, normalizedDate);
  // Parameter based entry isn't required here.
  return await sql.query`
     BEGIN TRANSACTION
          INSERT
            INTO
            user_note (updated_date,
            updated_user,
            version,
            external_note,
            internal_note,
            note_description,
            work_order_id,
            created_date ,
            created_user)
          values(${normalizedDate},
          ${username} ,
          0,
          1,
          0,
          ${note_description},
          ${wo_id},
          ${normalizedDate} ,
          ${username} )
          COMMIT
      `;
};

module.exports = addNoteQuery;
