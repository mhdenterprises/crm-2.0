const sql = require("mssql");
const dealerListQuery = async () => {
  return await sql.query(`
    SELECT name,client_name FROM mhd_dealer d ORDER BY d.client_name
  `);
};

module.exports = dealerListQuery;
