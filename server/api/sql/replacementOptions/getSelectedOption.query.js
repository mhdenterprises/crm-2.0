const sql = require("mssql");
const getSelectedOptionQuery = ({ id }) => {
  return sql.query`
    SELECT 
      rs.inventory_part_number, 
      wo.po_sr, 
      rs.customer_selected,
      rs.mfg,
      rs.model_number,
      rs.part_number,
      rs.cpu_model,
      rs.hdd,
      rs.ssd,
      rs.memory,
      rs.cost,
      rs.cpu_speed,
      rs.os,
      rs.version,
      rs.warranty,
      rs.lcd,
      rs.type,
      rs.has_data_plan,
      rs.replacement_selection_group_id
    FROM 
      work_order wo 
    INNER JOIN 
      replacement_selection_group rsg 
    ON 
      wo.id = rsg.work_order_id 
    INNER JOIN 
      replacement_selection rs 
    ON 
      rsg.id = rs.replacement_selection_group_id 
    WHERE 
      wo.id =  ${id} AND rs.customer_selected = 1
    ORDER BY rs.replacement_selection_group_id DESC
  `;
};

module.exports = getSelectedOptionQuery;
