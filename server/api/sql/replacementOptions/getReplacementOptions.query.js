const sql = require("mssql");
const getReplacementOptionsQuery = ({ workOrderId }) => {
  return sql.query`
        SELECT rs.cpu_model, rs.cpu_speed, rs.hdd, rs.lcd, 
        rs.memory, rs.mfg, rs.model_number, rs.os, rs.part_number, rs.resolution,
        rs.warranty, rs.product_description, rs.refurbished, rs.touch_screen, rs.ssd, rs.desktop, rs.tablet
        FROM replacement_selection rs
        INNER JOIN replacement_selection_group rsg
        ON rsg.id = rs.replacement_selection_group_id
        WHERE rsg.work_order_id = ${workOrderId}
        AND rsg.selection_group_status = 'CURRENT'
        ORDER BY rsg.updated_date DESC
      `;
};

module.exports = getReplacementOptionsQuery;
