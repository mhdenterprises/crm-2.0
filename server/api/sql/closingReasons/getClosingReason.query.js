const sql = require("mssql");

const getClosingReasonQuery = async () => {
  return await sql.query(`
    SELECT
        wsr.wo_sub_status_code ,
        wr.reason_text
    FROM
        wo_status_reason wsr
    JOIN wo_reason wr ON
        wsr.wo_reason_code = wr.reason_code
  `);
};

module.exports = getClosingReasonQuery;
