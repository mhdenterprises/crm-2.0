const sql = require("mssql");

const updateCustomerLogin = async ({ po_sr, updated_user }) => {
  return await sql.query(`
        BEGIN TRANSACTION 
        UPDATE CRM.CRM.customer_login SET login_open = 0, updated_date = GETDATE(), updated_user = '${updated_user}' WHERE user_name='${po_sr}'
        COMMIT
  `);
};

module.exports = updateCustomerLogin;
