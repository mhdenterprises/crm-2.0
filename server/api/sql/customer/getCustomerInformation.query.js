const sql = require("mssql");

const getCustomerInformationQuery = async ({ customerId }) => {
  return await sql.query`
    SELECT cc.first_name, cc.last_name, acc.address_line, acc.second_address_line, acc.city, acc.us_state, acc.postal_code,
    wo.mfg, wo.model, wo.cpu_model, lcd_size, ram, hard_drive
    FROM customer c
    INNER JOIN work_order wo
    ON c.id = wo.customer_id 
    INNER JOIN customer_contact cc
    ON cc.customer_id = c.id
    INNER JOIN address_customer_contact acc
    ON acc.customer_contact_id = cc.id
    WHERE c.id = ${customerId}
    AND cc.primary_contact = 1
    `;
};

module.exports = getCustomerInformationQuery;
