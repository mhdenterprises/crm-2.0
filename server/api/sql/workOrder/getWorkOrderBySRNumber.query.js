const sql = require("mssql");

const getWorkOrderBySRNumber = async (req) => {
  let wo_details = await sql.query(`
      SELECT
        wo.*,
        c.last_name,
        c.full_name,
        cc.email ,
        cc.primary_contact,
        acc.primary_phone,
        acc.address_line,
        acc.city,
        acc.us_state
      FROM
      work_order wo
      JOIN customer c ON
        wo.customer_id = c.id
      JOIN customer_contact cc ON
        c.id = cc.customer_id
      JOIN address_customer_contact acc ON
        cc.id = acc.customer_contact_id
      WHERE
        wo.po_sr = ${req.body.po_sr}
      ORDER BY
        wo.created_date DESC
      `);

  // Check if Work Order Details Exists
  let user_id = wo_details.recordset[0].id;

  // Search for User Notes
  let user_notes = await sql.query(`
        SELECT * FROM user_note un 
        WHERE work_order_id =  ${user_id}
        ORDER BY created_date DESC
      `);

  // Build wo_detail and notes
  let wo_detail = wo_details.recordset[0];
  let notes = user_notes.recordset;

  return {
    wo_detail,
    notes,
  };
};

module.exports = getWorkOrderBySRNumber;
