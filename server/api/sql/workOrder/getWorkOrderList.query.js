const sql = require("mssql");

const getWorkOrderListQuery = async (req) => {
  let conditionString = `
 WHERE c.first_name LIKE '${
   req.body.first_name || ""
 }%' AND c.last_name LIKE '${req.body.last_name || ""}%'${
    req.body.po_sr ? ` AND wo.po_sr = ${req.body.po_sr}` : ""
  }${
    req.body.primary_phone
      ? ` AND acc.primary_phone = ${req.body.primary_phone}`
      : ""
  }${req.body.email ? ` AND cc.email LIKE '${req.body.email || ""}%'` : ""}
${
  req.body.woSubStatus
    ? ` AND wo.wo_sub_status_code = '${req.body.woSubStatus}'`
    : ""
}
${req.body.woStatus ? ` AND wo.wo_status_code = '${req.body.woStatus}'` : ""}
${req.body.dealer ? ` AND wo.dealer = '${req.body.dealer}'` : ""}
${req.body.workOrderId ? ` AND wo.work_order_id = ${req.body.workOrderId}` : ""}
`;

  // Get the Work Order List after the Query
  let wo_details = await sql.query(`
    SELECT
    DISTINCT wo.*,
    c.last_name,
    c.full_name,
    cc.email ,
    acc.primary_phone,
    acc.mobile_phone,
    acc.home_phone,
    acc.address_line,
    acc.city,
    acc.us_state,
    acc.postal_code,
    cl.user_name,
    cl.encrypted_password
    FROM
    work_order wo
    JOIN customer c ON
    wo.customer_id = c.id
    LEFT JOIN customer_login cl ON
    cl.customer_id = c.id
    JOIN customer_contact cc ON
    c.id = cc.customer_id
    JOIN address_customer_contact acc ON
    cc.id = acc.customer_contact_id
    ${conditionString}
    ORDER BY
    wo.created_date DESC
    `);

  return wo_details;
};

module.exports = getWorkOrderListQuery;
