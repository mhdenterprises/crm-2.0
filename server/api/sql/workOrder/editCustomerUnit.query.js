const sql = require("mssql");
const editCustomerUnitQuery = async ({ id, query }) => {
  return await sql.query(`
      BEGIN TRANSACTION 
      UPDATE work_order SET ${query} WHERE id=${id}
      COMMIT
  `);
};

module.exports = editCustomerUnitQuery;
