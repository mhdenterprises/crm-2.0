const sql = require("mssql");

const updateWorkOrder = async ({ id, query }) => {
  // Make Sure to have the WHERE clause
  return await sql.query(`
       BEGIN TRANSACTION 
       UPDATE work_order SET ${query} WHERE id=${id}
       COMMIT
        `);
};

module.exports = updateWorkOrder;
