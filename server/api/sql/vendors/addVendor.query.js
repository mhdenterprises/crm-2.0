const sql = require("mssql");

const addVendorQuery = async ({
  username,
  vendorname,
  normalizedDate,
}) => {
  let vendorDetails = await sql.query(`
    SELECT vendor FROM vendor
    WHERE vendor = '${vendorname}'
  `);

  let vendorDetail = vendorDetails.recordset;

    // If Vendor already exists
    if (vendorDetail.length > 0) {
      return { error:"Given vendor name " + vendorname + " already exists. Try with different name." };
    }

    return await sql.query`
      BEGIN TRANSACTION
            INSERT
              INTO
              vendor (
                created_date,
                created_user,
                vendor)
              values(${normalizedDate},
              ${username},
              ${vendorname} ) 
            COMMIT
        `;
};

module.exports = addVendorQuery;
