const sql = require("mssql");

const portalLoginQuery = ({ username, portal_type }) => {
  return sql.query`
      SELECT c.encrypted_password, c.customer_id, wo.work_order_id, wo.wo_sub_status_code,
      cc.first_name, cc.last_name, acc.address_line, acc.second_address_line, acc.city, acc.us_state, acc.postal_code
      FROM customer_login c
      INNER JOIN work_order wo
      ON wo.customer_id = c.customer_id
      INNER JOIN customer_contact cc
      ON cc.customer_id = c.customer_id
      INNER JOIN address_customer_contact acc
      ON acc.customer_contact_id = cc.id
      WHERE c.user_name=${username} AND c.portal_type=${portal_type} AND c.login_open = 1 AND cc.primary_contact = 1
    `;
};

module.exports = portalLoginQuery;
