const swaggerAutogen = require("swagger-autogen")();

const doc = {
  info: {
    title: "CRM 2.0 API",
    description:
      "Private API of MHD Enterprises, interactive with internal database. It's used to handle portal and the new CRM interactions.",
  },
  host: "localhost:3001",
  schemes: ["http"],
};

const outputFile = "server/swagger/swagger-output.json";
const endpointsFiles = ["server/index.js"];

swaggerAutogen(outputFile, endpointsFiles, doc);
